/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 *
 * @author nam oio
 */
public class ProductCart {
     
    
    private final  List cartItems;
    public  ProductCart(){
        cartItems=new ArrayList();
    }
    private List product;
    
    
    public List getProducts(){
        List temp=new ArrayList();
        try{
             Connection connection = new DBConnection().getConnection();
       Statement s=connection.createStatement();
       ResultSet rs=s.executeQuery("select * from CART");
            while (rs.next()) {                
                Product item=new Product();
                item.setProductId(1);
                item.setProductName("b");
                item.setProductType("1");
                item.setPrice(1);
                System.out.println("tt");
               temp.add(item);
            }
        
        }
        catch(Exception e){
            
        }
        return temp;
    }
     public List<Product> getAccount() {
       
       
        
                try {
                    try {
                        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(ProductCart.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InstantiationException ex) {
                        Logger.getLogger(ProductCart.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(ProductCart.class.getName()).log(Level.SEVERE, null, ex);
                    }
                
            
            String url = "jdbc:sqlserver://localhost:1433;databaseName=sc";
            String username = "sa";
            String password = "123456";
            Connection connection = DriverManager.getConnection(url, username, password);
            System.out.println("Connected: " + connection.getClientInfo());
            java.sql.Statement st = connection.createStatement();
            String query = "select * from cart ";;

            ResultSet rs = st.executeQuery(query);
  List<Product> products = new LinkedList<>();
            if (rs != null) {
                while (rs.next()) {
                     Product item=new Product();
                item.setProductId(rs.getInt(1));
                item.setProductName(rs.getString(2));
                item.setProductType(rs.getString(3));
                item.setPrice((float) rs.getFloat(4));
                System.out.println("tt");
               products.add(item);

                }
            }

            rs.close();
            st.close();
            connection.close();
           
            return products;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    
    public List<Product> getall() {

        try {
            Connection connection = new DBConnection().getConnection();
            Statement stmt2 = connection.createStatement();
            ResultSet rs = stmt2.executeQuery("select * from CART");
            List<Product> products = new LinkedList<>();
            while (rs.next()) {
                
                 Product item=new Product();
                item.setProductId(rs.getInt(1));
                item.setProductName(rs.getString(2));
                item.setProductType(rs.getString(3));
                item.setPrice((float) rs.getFloat(4));
                System.out.println("tt");
               products.add(item);
            }
            return products;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
   
    
    public void addItem(int productId, String productName, String productType, float price,int quantity) {
        Product item=null;
        boolean match = true;
        for(int i=0;i<cartItems.size();i++){
            System.out.println(i+"+"+((Product) cartItems.get(i)).getProductId());
          if(((Product) cartItems.get(i)).getProductId()==productId)
          {
              item =(Product) cartItems.get(i);
              setAmount(getAmount()+quantity * item.getPrice());
              item.setQuantity(item.getQuantity()+quantity);
               
              match=false;
              break;
          }
        }
        if(match){
            System.out.println("cr");
            item=new Product();
            item.setProductId(productId);
            item.setPrice(price);
            item.setProductName(productName);
            item.setProductType(productType);
            item.setQuantity(quantity);
            setAmount(getAmount()+quantity * item.getPrice());
            cartItems.add(item);
        }
    }
    public void removeItem(int productId){
        for(int i=0;i<cartItems.size();i++){
            Product item=(Product) cartItems.get(i);
            if(item.getProductId()==productId){
                setAmount(getAmount()-item.getPrice()*item.getQuantity());
                cartItems.remove(i);
                break;
            }
        }
    }
      
    
    
       public int dem(){
          return cartItems.size();
            }
    public List getCartItems(){
        return cartItems;
    }
    
    private float amount;
    public float getAmount(){
        return this.amount;
    }
    
    public void setAmount(float amount){
        this.amount=amount;
    }
}
