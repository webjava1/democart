<%-- 
    Document   : checkout
    Created on : Mar 20, 2018, 4:22:37 PM
    Author     : nam oio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="prod" class="model.ProductCard" scope="session"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <center>
        <table>
           <tr>
            <td>
                <b> Product Id</b></td>
            <td><b> Product Name</b></td>
            <td><b> Product Type</b></td>
            <td><b> Price </b></td>
            <td><b> Quantity </b></td>
        </tr>
        
         <c:forEach var="item" items="${prod.cart.Items}">
        <tr>

            <td><${item.productId}/td>
            <td><${item.productName}/td>
            <td><${item.productType}/td>

            <td><${item.price}/td>
            <td><${item.quantity}/td>
            
        </tr>
    </c:forEach>
        <td></td>
        <td></td>
        <td><b>Total</b></td>
        <td><b>${prod.amount}</b></td>
        </table>
        <br>
        <a href="index.jsp">Home</a>
        
    </center>
    </body>
</html>
