<%-- 
    Document   : index
    Created on : Mar 20, 2018, 4:20:16 PM
    Author     : nam oio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="prod" class="model.ProductCart" scope="session"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
     
        <form action="ShoppingServlet" name="shoppingForm" method="post">
            <b>Product</b>  
            <select name="products">
                <c:forEach var="item" items="${prod.account}">
                    <option>
                        
                   
                        ${item.productId}${"|"}${item.productName}${"|"}${item.productType}${"|"}${item.price}${"|"}
                         </option>
                   
                </c:forEach>
            </select>
            <br>
            <br>
            <b>Quanity</b><input type="text" name="qty" value="1"><br>
            <input type="hidden" name="action" value="ADD"/>
            <input type="submit" name="Sumbit" value="Add to Cart"/>
        </form>
        <p>${mesage}</p>
        <jsp:include page="cart.jsp" flush="true" />
    </body>
</html>
